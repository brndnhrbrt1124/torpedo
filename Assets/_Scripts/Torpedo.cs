﻿using UnityEngine;
using System.Collections;

public class Torpedo : MonoBehaviour {

	public GameObject torpedo;

	public float time = Time.deltaTime;

	public int startingX = 0;
	public int startingY = 18;
	public int startingZ = -410;

    public float gravity = -9.8f;
    public float accelX = 0.0f;
    public float accelZ = 0.0f;

    public float startingVelY = 0.0f;
    private float velY;

    public float startingVelX = 0.0f;
    private float velX;

    public float startingVelZ = 0.0f;
    private float velZ;

    void OnCollisionEnter(Collision col) {
        if (col.gameObject.tag == "Floor") {
            Destroy(torpedo);
            print("You hit the floor, you lose!");
            Application.LoadLevel(Application.loadedLevel);
        } else if(col.gameObject.tag == "Wall") {
            Destroy(torpedo);
            print("You hit the wall, you win!");
            Application.LoadLevel(Application.loadedLevel);
        }
    }

    void Start() {
        velY = startingVelY;
        velX = startingVelX;
        velZ = startingVelZ;

        torpedo.transform.position = new Vector3 (startingX, startingY, startingZ);
	}

	// Update is called once per frame
	void Update () {
        velY = findVel(velY, gravity, time);
        velX = findVel(velX, accelX, time);
        velZ = findVel(velZ, accelZ, time);
        torpedo.transform.position = findPosition();
    }

    private float findVel(float vel, float accel, float time) {
        float v1 = vel + (accel * time);
        return v1;
    }

    private Vector3 findPosition() {

        var posY = torpedo.transform.position.y;
        float half = 0.5f;
        float t2 = time * time;
        float finalY = posY + (velY * time) + (half * gravity * t2);

        var posX = torpedo.transform.position.x;
        half = 0.5f;
        t2 = time * time;
        float finalX = posX + (velX * time) + (half * accelX * t2);

        var posZ = torpedo.transform.position.z;
        half = 0.5f;
        t2 = time * time;
        float finalZ = posZ + (velZ * time) + (half * accelZ * t2);

        return new Vector3(finalX, finalY, finalZ);
	}
}
