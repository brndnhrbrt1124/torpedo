﻿using UnityEngine;
using System.Collections;

public class GhostCamera: MonoBehaviour {
	public float initSpeed = 5f;
	public float boostSpeed = 1.25f;
	public bool movement = true;
	public bool rotation = true;
	public float cursorSensitivity = 0.020f;
	public bool cursorToggleAllowed = true;

	public KeyCode forward = KeyCode.W;
	public KeyCode backward = KeyCode.S;
	public KeyCode right = KeyCode.D;
	public KeyCode left = KeyCode.A;
	public KeyCode cursorToggle = KeyCode.Escape;

	private float speed = 0f;
	private bool moving = false;
	private bool toggled = false;

	private void OnEnable() {
		if(cursorToggleAllowed) {
			Screen.lockCursor = true;
			Cursor.visible = false;
		}
	}

	private void Update() {
		if(movement) {
			bool wasMoving = moving;
			Vector3 deltaPosition = Vector3.zero;

			if(moving)
				speed += boostSpeed * Time.deltaTime;

			moving = false;

			isMoving(forward, ref deltaPosition, transform.forward);
			isMoving(backward, ref deltaPosition, -transform.forward);
			isMoving(right, ref deltaPosition, transform.right);
			isMoving(left, ref deltaPosition, -transform.right);

			if(moving) {
				if(moving != wasMoving)
					speed = initSpeed;
				transform.position += deltaPosition * speed * Time.deltaTime;
			} else  {
				speed = 0f;
			}
		}

		if(rotation) {
			Vector3 angles = transform.eulerAngles;
			angles.x += -Input.GetAxis("Mouse Y") * 359f * cursorSensitivity;
			angles.y += Input.GetAxis("Mouse X") * 359f * cursorSensitivity;
			transform.eulerAngles = angles;
		}

		if(cursorToggleAllowed) {
			if(Input.GetKey(cursorToggle)) {
				if(!toggled) {
					toggled = true;
					Screen.lockCursor = !Screen.lockCursor;
					Cursor.visible = !Cursor.visible;
				}
			} else {
				toggled = false;
			}
		} else {
			toggled = false;
			Cursor.visible = false;
		}
	}

	private void isMoving(KeyCode keyCode, ref Vector3 deltaPosition, Vector3 directionVector) {
		if(Input.GetKey(keyCode)) {
			moving = true;
			deltaPosition += directionVector;
		}
	}
}